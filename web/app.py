from flask import Flask, render_template, redirect, url_for, request, jsonify
import json
from pymongo import MongoClient, ASCENDING, errors
import requests as req
app = Flask(__name__)

client = MongoClient('mongodb://admin:myadminpassword@139.99.45.55/')#MongoClient('mongodb://localhost/')#
db = client.casambi_database
db.network_info.create_index([("username", ASCENDING), ("username_Network", ASCENDING), ("password_Network", ASCENDING)],unique=True)

api_key = "P8guJqZqpPhHHWdCYvWNV5KmvZrdQKHBrvTcBDX45fRbBJWLJnnhmRdyZEJajfZs"

@app.route('/home')
def hello_world():
    return 'Hello, World!'
@app.route('/success')
def Success():
    return 'Newtork Information Saved.'


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None



    if request.method == 'POST':
        form_data = request.form

        body = {
            "email": str(form_data['username_Network']),
            "password": str(form_data['password_Network']),
            }
        api_header = {
            "X-Casambi-Key":"P8guJqZqpPhHHWdCYvWNV5KmvZrdQKHBrvTcBDX45fRbBJWLJnnhmRdyZEJajfZs"
        }
        
        try:
            r = req.post("https://door.casambi.com/v1/networks/session", json = body, headers = api_header )
            app.logger.debug(r.status_code)
            if  not (r.status_code == req.codes.ok ):
                error = "Please check the network credentials."

            else:
                #sess_id = r.text.
                data = dict(form_data)
                app.logger.debug(data)
                try:
                    db.network_info.insert_one(data)
                except errors.DuplicateKeyError:
                    return render_template('login.html', error="Network Already exist.")
                
                #return "Authorized."  + str(network_id) + str(sess_id)
                return render_template('login.html', error="Information Saved")
                 
        except:
            error = "Error in Authorization Step."
            pass        


            
    return render_template('login.html', error=error)


if __name__ == "__main__":
    app.run(host = "0.0.0.0", port=5005)